let toggleButton = document.querySelector('.toggle-button');
let mobileNav = document.querySelector('.top-menu');
console.log(toggleButton);
console.log(mobileNav);

toggleButton.addEventListener('click', function() {
    if (mobileNav.style.display === 'block') {
        mobileNav.style.display = 'none';
    } else {
        mobileNav.style.display = 'block';
    }
});